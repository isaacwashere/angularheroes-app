# Angular Tour Of Heroes/Jedi App

This is a small app that uses some basic Angular tools to show some mock Jedi data

## How to Run

1. Navigate to Terminal

> open directory in your Terminal

2. Start the server

> Run `ng serve`

3. View the app

> Navigate to `http://localhost:4200/`

## Abilities & Features

- A user can view Jedi ID, and name information, and Bio
- A user can click a Jedi tab and view the details
- A user can view messages

## Notable Technologies/Functions/Methods

| Technology/Function |            Purpose             |
| ------------------- | :----------------------------: |
| Routing             | Manage user routes/url changes |

## Future Implementation(s)

- Make a request for Jedi data from Star Wars API
- For each Jedi display (in the details) their most rival-ed Sith
- Edit the colors so they match Star Wars look/feel
- Edit the individual Jedi so their tab matches the color of their lightsaber
