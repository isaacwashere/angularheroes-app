import { InMemoryDbService } from "angular-in-memory-web-api";
import { Hero } from "./hero";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      {
        id: 10292,
        name: "Yoda",
        bio:
          "Yoda was a legendary Jedi Master and stronger than most in his connection with the Force. Small in size but wise and powerful, he trained Jedi for over 800 years, playing integral roles in the Clone Wars, the instruction of Luke Skywalker, and unlocking the path to immortality"
      },
      {
        id: 11290,
        name: "Mace Windu",
        bio:
          "A grim Jedi Master with an amethyst-bladed lightsaber, Mace Windu was the champion of the Jedi Order, with little tolerance for the failings of the Senate, the arguments of politicians, or the opinions of rebellious Jedi."
      },
      {
        id: 11293,
        name: "Obi-Wan Kenobi",
        bio:
          "A legendary Jedi Master, Obi-Wan Kenobi was a noble man and gifted in the ways of the Force. He trained Anakin Skywalker, served as a general in the Republic Army during the Clone Wars, and guided Luke Skywalker as a mentor"
      },
      {
        id: 12295,
        name: "Luke Skywalker",
        bio:
          "Luke Skywalker was a Tatooine farmboy who rose from humble beginnings to become one of the greatest Jedi the galaxy has ever known. Along with his friends Princess Leia and Han Solo, Luke battled the evil Empire, discovered the truth of his parentage, and ended the tyranny of the Sith"
      },
      {
        id: 13297,
        name: "Anakin Skywalker*",
        bio:
          "Discovered as a slave on Tatooine by Qui-Gon Jinn and Obi-Wan Kenobi, Anakin Skywalker had the potential to become one of the most powerful Jedi ever, and was believed by some to be the prophesied Chosen One who would bring balance to the Force."
      },
      {
        id: 14299,
        name: "Qui-Gon Jinn",
        bio:
          "A venerable if maverick Jedi Master, Qui-Gon Jinn was a student of the living Force. Qui-Gon lived for the moment, espousing a philosophy of 'feel, don't think -- use your instincts.' On Tatooine, Qui-Gon discovered a young slave boy named Anakin Skywalker who was strong in the Force"
      },
      {
        id: 15291,
        name: "Ki-Adi-Mundi",
        bio:
          "Ki-Adi-Mundi was a Cerean alien who sat on the Jedi Council. A humanoid being, Ki's most distinguishing physical feature was an enlarged conical cranium that contained a binary brain."
      },
      {
        id: 16293,
        name: "Luminara Unduli",
        bio:
          "A green-skinned Mirialan, Luminara Unduli served the Jedi Order during the final years of the Galactic Republic, and trained the capable Padawan Barriss Offee. A no-nonsense Jedi Master, Luminara fought in Clone Wars battles such as Geonosis and Kashyyyk, and was thought to have perished during Order 66."
      },
      {
        id: 17295,
        name: "Adi Gallia",
        bio:
          "Adi Gallia was a female Tholothian Jedi Master during the twilight years of the Galactic Republic. Gallia was a respected member of the Jedi High Council, who served on that august body in the years leading up to the Clone Wars, as well as some time into that devastating conflict."
      },
      {
        id: 18297,
        name: "Aayla Secura",
        bio:
          "Aayla Secura stood out among the many faces of the Jedi ranks. A cunning warrior and Jedi Knight during the rise of the Clone Wars, Aayla fought alongside Clone Commander Bly on many exotic battlefields."
      },
      {
        id: 19299,
        name: "Shaak Ti",
        bio:
          "A wise and patient Jedi Master, the Togruta Shaak Ti fought at the Battle of Geonosis, and supervised the training of clone cadets on Kamino. There, she struggled to balance the necessity of producing capable troopers with compassion for the clones as living beings."
      },
      {
        id: 20291,
        name: "Kseniia Karaguna",
        bio:
          "Born in Ukagara, to a family of criminal expertise, Kseniia was a wise and strategically gifted Jedi. Thought to have passed during the clone ambush delivered by Order 66, she left a remarkable influence on the Jedi strategic code and ways, manuevers that have become standardized accross the galaxy."
      },
      {
        id: 21293,
        name: "Rey",
        bio:
          "Rey is a Jakku scavenger, a survivor toughened by life on a harsh desert planet. When the fugitive droid BB-8 appeals to her for help, Rey finds herself drawn into a galaxy-spanning conflict. Despite dismissing herself as 'no one,' she learns that her life is being shaped by the mysterious power of the Force."
      },
      {
        id: 22295,
        name: "Anastasia Kriznallia",
        bio:
          "Anastasia, or Ann (pronounced ahh-n), was one of the most successful liason between the Jedi and the Senate, as well as the Business world. As powerful a Jedi as any, she was uniquely skilled at insuring the galaxies many planets understood the importance of the Jedi and their role in peacemaking. But she was also not afraid to do battle, when deemed necessary."
      },
      {
        id: 23297,
        name: "Icalunas Cuatanaz",
        bio: "No Known information exists."
      }
    ];
    return { heroes };
  }

  genId(heroes: Hero[]): number {
    return heroes.length > 0
      ? Math.max(...heroes.map(hero => hero.id)) + 1
      : 16;
  }

  constructor() {}
}
