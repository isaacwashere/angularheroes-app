import { Hero } from "./hero";

export const HEROES: Hero[] = [
  { id: 10292, name: "Yoda" },
  { id: 11290, name: "Mace Windu" },
  { id: 11293, name: "Obi-Wan Kenobi" },
  { id: 12295, name: "Luke Skywalker" },
  { id: 13297, name: "Anakin Skywalker*" },
  { id: 14299, name: "Qui-Gon Jinn" },
  { id: 15291, name: "Ki-Adi-Mundi" },
  { id: 16293, name: "Luminara Unduli" },
  { id: 17295, name: "Adi Gallia" },
  { id: 18297, name: "Aayla Secura" },
  { id: 19299, name: "Shaak Ti" },
  { id: 20291, name: "Kseniia" },
  { id: 21293, name: "Rey" },
  { id: 22295, name: "Anastasia" },
  { id: 23297, name: "Isaac" }
];
